package ru.tankist88.gfskmod;

public class ConvertResult {
    private double[] array;
    private int shiftCounts;

    public double[] getArray() {
        return array;
    }

    public void setArray(double[] array) {
        this.array = array;
    }

    public int getShiftCounts() {
        return shiftCounts;
    }

    public void setShiftCounts(int shiftCounts) {
        this.shiftCounts = shiftCounts;
    }
}
