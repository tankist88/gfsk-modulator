package ru.tankist88.gfskmod;

import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;

public class Main {
    private static final String OUT_FILE = "/home/tankist/out_c.raw";
    private static final String GB_FILE = "/home/tankist/gauss_bit.raw";
    private static final String SIG_FILE = "/home/tankist/signal1.raw";

    private static final int SPS = 20; // samples per symbol
    private static final double B = 0.14; // gaussian -3db bandwidth
    private static final double T_B = 0.000001; // bit time
    private static final double D_T = T_B / ((double) SPS); // time increment
    private static final double START_TIME = D_T; // start time
    private static final double INDEX_K = 1.0 / D_T; // pulse index koef
    private static final double CARRIER = 2 * Math.PI * 2402000000.0; // carrier frequency
//    private static final double CARRIER = 2 * Math.PI * 1.0; // carrier frequency
    private static final double K_F = 2 * Math.PI * 2000000.0; // K_F

    private static final double M_2_PI = 6.28318530717958647692; // 2*PI
    private static final double SQRT_M_2_PI = 2.50662827463100050241471; // square root of 2*PI
    private static final double SQRT_LN_2 = 0.8325546111576977507; // square root of ln2

    public static void main(String agrs[]) throws Exception {
        File signalFile = new File(SIG_FILE);

        FileInputStream fis = new FileInputStream(signalFile);

        int size = (int) signalFile.length();
        byte[] data = new byte[size];

        int read = fis.read(data);

        fis.close();

        if(read != size) {
            throw new IllegalStateException("Signal length is " + size + " bytes, but read only " + read + " bytes");
        }

        ConvertResult result = convert(SPS, data);
        double[] bitArray = result.getArray();
//        int shiftCount = result.getShiftCounts();

        double sigma = sigma(B);
        double kSigma = (1.0 / (SQRT_M_2_PI * sigma));

        FileOutputStream gBos = new FileOutputStream(new File(GB_FILE));
        for (int i = 0; i < bitArray.length; i++) {
            double b = bitArray[i];
            bitArray[i] = (b * gaussianFilter(b, sigma, kSigma));
            gBos.write(float2ByteArray((float) bitArray[i]));
        }
        gBos.close();

        FileOutputStream fos = new FileOutputStream(new File(OUT_FILE));
        double t = START_TIME;
        double phase = 0.0;

//        double expectedEndTime = (Byte.SIZE * size * T_B) + (shiftCount * (T_B - D_T)) + START_TIME;

        float[] transferData = new float[bitArray.length];

        while(t < D_T * bitArray.length) {
            phase += integration(t - D_T, t, bitArray);
            float val = (float) ( 0.9 * Math.cos(CARRIER * t + K_F * phase));
            transferData[(int) (Math.round(t * INDEX_K) - START_TIME)] = val;
            t += D_T;
        }

        long start = System.currentTimeMillis();
        for (float tData : transferData) {
            // TODO Вызывать hackRF One для отправки передачи сигнала
            fos.write(float2ByteArray(tData));
        }
        long elapsed = System.currentTimeMillis() - start;

        System.out.println("Elapsed " + elapsed + " ms");

        fos.close();
    }

    private static byte [] float2ByteArray (float value) {
        byte[] result = ByteBuffer.allocate(4).putFloat(value).array();
        ArrayUtils.reverse(result);
        return result;
    }

    private static ConvertResult convert(int samples, byte...bs) {
        ConvertResult convertResult = new ConvertResult();
        double[] tmp = new double[Byte.SIZE * bs.length * 4 * samples];
        int resultSize = 0;
        int offset = 0;
        int sampOffset = 0;
        int shiftCount = 0;
        for (int k = 0; k < bs.length; k++) {
            for (int i = 0; i < Byte.SIZE; i++) {
                double currentBit = (bs[k] >> i & 0x1) == 0x0 ? 1.0 : -1.0;
                double prevBit;
                if(k > 0 && i == 0) {
                    prevBit = (bs[k - 1] >> (Byte.SIZE - 1) & 0x1) == 0x0 ? 1.0 : -1.0;
                } else if(i > 0) {
                    prevBit = (bs[k] >> (i - 1) & 0x1) == 0x0 ? 1.0 : -1.0;
                } else {
                    prevBit = currentBit;
                }

                if(prevBit != currentBit) {
                    shiftCount++;
                    for(int j = 0; j < samples - 1; j++) {
                        double step = (j + 1) * (2.0 / samples);
                        tmp[i + offset + sampOffset + j] = (currentBit > 0) ? prevBit + step : prevBit - step;
                        resultSize++;
                    }
                    sampOffset += samples - 1;
                }
                for(int j = 0; j < samples; j++) {
                    tmp[i + offset + sampOffset + j] = currentBit;
                    resultSize++;
                }
                sampOffset += samples - 1;
            }
            offset += Byte.SIZE;
        }
        double[] result = new double[resultSize];
        System.arraycopy(tmp, 0, result,0, resultSize);

        convertResult.setArray(result);
        convertResult.setShiftCounts(shiftCount);

        return convertResult;
    }

    private static double sigma(double B) {
        return SQRT_LN_2 / (M_2_PI * B);
    }

    private static double gaussianFilter(double x, double sigma, double kSigma) {
        return kSigma * Math.exp(-0.5 * (x / sigma) * (x / sigma));
    }

    private static double integration(double a, double b, double[] bitArray) {
        if(a >= b) return 0.0;

        double interval = (b - a);
        double x0 = a, x1 = a + interval, sum = 0.0;

        while(x1 <= b) {
            sum += ((pulse(x0, bitArray) + pulse(x1, bitArray)) / 2.0) * (x1 - x0);
            x0 = x1;
            x1 += interval;
        }

        return sum;
    }

    private static double pulse(double tau, double[] bitArray) {
        return bitArray[(int) (Math.round(tau * INDEX_K) - START_TIME)];
    }
}
